<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>My Calculator</title>
<link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
<link rel="stylesheet" href="/myCalculator/assets/styles.css">

</head>
<body>

<div class="container">
  
        
<form method="POST" name="addform" action="save_calc.php">  
        <centre><label >History:</label></centre>
            <select id="update" name = "sts" onchange="rePrint()" value="<select>">
                <?php
                    $host        = "host = 127.0.0.1";
                    $port        = "port = 5432";
                    $dbname      = "dbname = calculatorDB";
                    $credentials = "user = postgres password=root";

                    $db = pg_connect( "$host $port $dbname $credentials"  );
                    $sql =<<<EOF
                        
                        select * from calc_table;
                    EOF;

                    $ret = pg_query($db, $sql);
                while($row = pg_fetch_row($ret)) {
                    //close while loop
                ?> 
                    <option value="<?php echo $row[1].' '.$row[3].' '.$row[2]; ?>"><?php echo $row[1]." ".$row[3]." ".$row[2]; ?></option>
                <?php
                    } //close option tag
                ?>
            </select>

        <input type="text" id="num1" name="num1" placeholder="input 1" value="<?php echo $gname;?>" required>

        <input type="text" id="num2" name="num2" placeholder="input 2" value="<?php echo $gname;?>" required>

        <input type="hidden" id="op_id" name="myhidden" value=""><br><br>

        Result<input type="text" id="result" name="result" placeholder="Result" value="<?php echo $gname;?>" readonly><br>

        <table id ="operators" border="1">
                <tr>
                    <td><button type="button" onclick="calculation('+');">+</button> </td>
                    <td><button type="button" onclick="calculation('-');">-</button> </td>
                </tr>
                <tr>
                    <td><button type="button" onclick="calculation('*');">*</button> </td>
                    <td><button type="button" onclick="calculation('/');">/</button> </td>
                </tr>
        </table>
        <input type="submit" value="Save Calculation"> 
        <button type="button" onclick="calculation('c');" border="1">AC</button>  

</form>

</div>

<script src="/myCalculator/assets/js.js"> </script>
</body>
</html>